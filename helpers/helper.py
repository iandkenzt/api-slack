from datetime import datetime, timedelta
from dateutil import tz
from exceptions import BadRequest
from .pydb import mongo
import random
import string
import time


class Helper(object):

    def get_current_date(self, format=None) -> dict:
        """Get current date.

        get_current_date()

        Returns:
            {
                "date": "2018-05-28T06:29:32+0000",
                "timestamp": 1527463772.788689
            }
        """

        utc_now = datetime.now().utcnow()
        if format:
            date = utc_now.strftime(format)
        else:
            date = utc_now.strftime('%Y-%m-%dT%H:%M:%S+0000')
        timestamp = utc_now.timestamp()

        result = {
            'date': date,
            'timestamp': timestamp,
        }

        return result

    def get_current_date_timezone(self, timezone_name: str, format=None) -> dict:
        """Get current date by timezone name.

        Args:
            timezone_name = "Asia/Jakarta"

        get_current_date_timezone("Asia/Jakarta")

        Returns:
            {
                "date": "2018-05-28T13:22:39+0000",
                "hour": "01:22 PM",
                "timestamp": 1527488559.330734
            }
        """

        utc_now 		= datetime.now().utcnow()
        timezone_now 	= self.convert_date_to_timezone(utc_now, timezone_name)
        if format:
            date = timezone_now.strftime(format)
        else:
            date = timezone_now.strftime('%Y-%m-%dT%H:%M:%S+0000')

        # generate timestamp
        timestamp = timezone_now.timestamp()

        # generate hour
        hour = timezone_now.strftime('%I:%M %p')

        result = {
            'date': date,
            'hour': hour,
            'timestamp': timestamp,
        }

        return result

    def convert_string_to_datetime(self, date_string, format='%Y-%m-%dT%H:%M:%S+0000') -> dict:
        """Convert date string to datetime.

        Args:
            date_string = "2018-05-28T13:22:39+0000"

        convert_string_to_datetime("2018-05-28T13:22:39+0000")

        Returns:
            {
                "datetime": "Mon, 28 May 2018 13:22:39 GMT",
                "timestamp": 1527488559
            }
        """

        formatted = datetime.strptime(date_string, format)

        result = {
            "datetime": formatted,
            "timestamp": formatted.timestamp()
        }
        return result

    def get_current_timestamp(self) -> float:
        """Get current timestamp.

        get_current_timestamp()

        Returns: 1527464268.734765
        """

        return datetime.now().utcnow().timestamp()

    def convert_date_to_timezone(self, current_date: 'datetime', timezone_name: 'str'):
        """Convert current date string to timezone.

        Args:
            current_date = "2018-05-28T13:22:39+0000"
            timezone_name = "Asia/Jakarta"

        Returns:
            Mon, 28 May 2018 06:50:25 GMT

        """

        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz(timezone_name)

        current_date = datetime.now().utcnow()

        utc = current_date.replace(tzinfo=from_zone)
        current_date = utc.astimezone(to_zone)

        return current_date

    def convert_time_to_unix_timestamp(self, time_string):
        """Convert time to unix timestamp.

        Args:
            time_string = 2018-05-28

        convert_time_to_unix_timestamp("2018-05-28")

        Returns:
            1527440400
        """
        # convert to unix_timestamp from string
        time_string = datetime.today().strftime("%Y-%m-%d")
        unix_timestamp = int(time.mktime(datetime.strptime(time_string, "%Y-%m-%d").timetuple()))

        return unix_timestamp

    def convert_time_to_unix_timestamp_iso_format(self, time_string: str) -> int:
        """Convert time to unix timestamp iso format.

        Args:
            time_string = "2018-05-28T13:22:39+0000"

        convert_time_to_unix_timestamp_iso_format("2018-05-28T13:22:39+0000")

        Returns:
            1527488559

        """
        unix_timestamp = int(time.mktime(datetime.strptime(time_string, "%Y-%m-%dT%H:%M:%S%z").timetuple()))

        return unix_timestamp

    def convert_timestamp_to_timezone(self, timestamp: 'int', timezone_name: 'str'):
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz(timezone_name)

        date = datetime.fromtimestamp(timestamp)

        utc = date.replace(tzinfo=from_zone)
        date = utc.astimezone(to_zone)

        return date

    def get_range_date(self, start_date: 'str', end_date: 'str'):
        """Get iterable date between start_date and end_date

        Args:
            start_date: start date to be sync
            end_date: end date to be sync

        Returns:
            list
        """
        if end_date is None:
            end_date = start_date

        start_date = datetime.strptime(start_date, "%Y%m%d")
        end_date = datetime.strptime(end_date, "%Y%m%d")
        delta = (end_date - start_date).days

        for i in range(delta + 1):
            date = (end_date - timedelta(days=i))
            yield date

    def get_range_date_difference(self, start_date: 'str', end_date: 'str'):
        """Get iterable date between start_date and end_date

        Args:
            start_date: start date to be sync
            end_date: end date to be sync

        Returns:
            list
        """
        if end_date is None:
            end_date = start_date

        start_date = datetime.strptime(start_date, "%Y-%m-%d")
        end_date = datetime.strptime(end_date, "%Y-%m-%d")
        delta = (end_date - start_date).days + 1

        return delta

    def list_date(self, start_date, end_date):
        start_date = datetime.strptime(start_date, "%Y-%m-%d")
        end_date = datetime.strptime(end_date, "%Y-%m-%d")
        date_list = []
        for n in range(int((end_date - start_date).days + 1)):
            each_day = start_date + timedelta(n)
            date_list.append(each_day.strftime("%Y-%m-%d"))

        return date_list

    def get_today_and_seven_days_before(self):
        """Get iterable date between today and seven days before

        Args:

        Returns:
            string
        """
        today = datetime.today()
        seven_days_before = (today - timedelta(days=6)).strftime("%Y-%m-%d")
        today = today.strftime("%Y-%m-%d")
        list_seven_days = self.list_date(seven_days_before, today)
        return today, seven_days_before, list_seven_days

    def today_date_check(self, since_date, until_date):
        today = datetime.today().strftime("%Y-%m-%d")
        unix_timestamp_today = self.convert_time_to_unix_timestamp(today)
        start_time = self.convert_time_to_unix_timestamp(since_date)
        end_time = self.convert_time_to_unix_timestamp(until_date)
        if start_time > end_time:
            raise BadRequest("date since is more than until date!", 200, 2)

        if start_time > unix_timestamp_today:
            since_date = since_date
            # raise BadRequest("date since is more than today!", 200, 1)

        if end_time > unix_timestamp_today:
            until_date = until_date

        return since_date, until_date

    def start_time_check(self, start_time):
        current = time.time()
        if current < start_time:
            return True

        return False

    def end_time_check(self, end_time):
        result = False
        today = datetime.today().strftime("%Y-%m-%d")
        unix_timestamp_today = self.convert_time_to_unix_timestamp(today)

        if end_time < unix_timestamp_today:
            result = True

        return result

    def allowed_image_format(self, filename):
        # For a given file, return whether it's an allowed type or not
        image_formats = set(['png', 'jpg', 'jpeg', 'gif'])

        return '.' in filename and \
               filename.rsplit('.', 1)[1].lower() in image_formats

    def allowed_video_format(self, filename):
        video_formats = set([
            '3g2', '3gp', '3gpp', 'asf', 'avi', 'dat', 'divx', 'dv', 'f4v', 'flv', 'm2ts', 'm4v', 'mkv', 'mod', 'mov', 'mp4', 'mpe', 'mpeg', 'mpeg4', 'mpg', 'mts', 'nsv', 'ogm', 'ogv', 'qt', 'tod', 'ts', 'vob', 'wmv'
        ])

        return '.' in filename and \
               filename.rsplit('.', 1)[1].lower() in video_formats

    def random_string(self):
        size = 12
        chars = string.ascii_lowercase + string.digits
        return ''.join(random.choice(chars) for i in range(size))

    def get_json_key_value(self, key, json, return_value=False):

        if key in json:
            return_value = json[key]

        return return_value

    def print_log(self, message, log_type="INFO"):
        print("{0} [{1}] {2}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), log_type, message))

    def get_next_id(self, name: str) -> int:

        try:
            query = {'_id': name}
            counter = mongo.db.mongoengine.counters.find_one_and_update(
                query, update={
                    '$inc': {
                        'next': 1
                    }
                }, upsert=True).get('next')
        except Exception:
            counter = 1

        return counter

    def exception(self, e):
        try:
            error_body 		= e.body()
            error_body_dict = error_body["error"]
        except:
            try:
                error_body_dict = e['error']
            except:
                print("[HELPER] exception()")
                print("[HELPER] Cant parse body")
                print("[HELPER] Print Error: ")
                print(e)
                error_body_dict = {}

        error_code 		= ''
        error_subcode 	= ''
        error_message 	= ''
        error_title 	= ''
        error_user_msg	= ''

        if "code" in error_body_dict:
            error_code 	= int(error_body_dict["code"])
        if "error_subcode" in error_body_dict:
            error_subcode 	= int(error_body_dict["error_subcode"])
        if "message" in error_body_dict:
            error_message 	= str(error_body_dict["message"])
        if "error_user_title" in error_body_dict:
            error_title 	= str(error_body_dict["error_user_title"])
        if "error_user_msg" in error_body_dict:
            error_user_msg 	= str(error_body_dict["error_user_msg"])

        res = {}
        res['code'] = error_code
        res['subcode'] = error_subcode
        res['message'] = error_message
        res['title'] = error_title
        res['user_msg'] = error_user_msg
        res['body'] = error_body_dict

        return res
