from settings.configuration import Configuration
from helpers import Helper
from helpers.pydb import mongo
from exceptions import BadRequest

import json
import pymongo
import requests

conf = Configuration()
helper = Helper()


class AuthController(object):

    def __init__(self, **kwargs):
        pass

    def auth_test(self) -> bool:
        # prepare requests auth test
        host = "{}/auth.test".format(conf.SLACK_HOST)
        data = {"token": conf.SLACK_LEGACY_TOKEN}
        req = requests.post(host, data)

        # parsing response to json
        res_json = req.json()

        # handle requests
        response = {}
        if "error" in res_json:
            response["data"] = res_json["error"]
            response["error"] = True
        else:
            response["data"] = res_json
            response["error"] = False

        # convert response to object
        response = json.loads(json.dumps(response))

        return response
