from flask import Blueprint, jsonify, request

from controllers import AuthController
from helpers import Helper
from helpers.decorator import secret_key_required

auth_ctrlr = AuthController()
helper = Helper()

bp = Blueprint(__name__, 'auth')


@bp.route("/auth/test", methods=["POST"])
@secret_key_required()
def auth_test():
    """Auth test
    This endpoint for checks authentication and tells "you" who you are, even if you might be a bot.
    You can also use this method to test whether Slack API authentication is functional.

    example ::
        >> curl -i -X POST
            -H "Content-Type: application/json"
            -H "x-api-key: <app secret key>"
            http://localhost:5000/auth/test

    endpoint ::
        POST /auth/test

    **success response**

    .. sourcecode:: http
        HTTP/1.1 200 OK
        Content-Type: text/javascript

        {
            "data": {
                "ok": true,
                "team": "XXXXXXXXX",
                "team_id": "XXXXXXXXX",
                "url": "https://XXXXXXXXX.slack.com/",
                "user": "ABCDEFG",
                "user_id": "XXXXXXXXX"
            },
            "error": 0,
            "message": "Success"
        }

    """

    # auth test
    auth_test = auth_ctrlr.auth_test()

    # default response format
    response = {"error": 0, "data": [], "message": "Success"}

    if auth_test["error"] is True:
        response["error"] = 1
        response["message"] = auth_test["data"]
    else:
        response["data"] = auth_test["data"]

    return jsonify(response)
