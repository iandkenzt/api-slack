from settings.configuration import Configuration
from helpers import Helper
from helpers.pydb import mongo
from exceptions import BadRequest

import json
import pymongo
import requests

conf = Configuration()
helper = Helper()


class ChannelController(object):

    def __init__(self, **kwargs):
        pass

    def channel_create(self, json_data: dict) -> bool:

        # validation json data
        try:
            channel_name = json_data["channel_name"]
        except:
            raise BadRequest("channel_name is missing", 200, 1)

        # prepare requests for create channel to Slack API
        host = "{}/channels.create".format(conf.SLACK_HOST)
        params = {
            "token": conf.SLACK_LEGACY_TOKEN,
            "name": channel_name,
            "validate": True
        }
        req = requests.get(host, params=params)

        # parsing response to json
        res_json = req.json()

        # handle detail message if failed to create channel
        if "error" in res_json:
            return False, res_json["detail"]

        return True, None

    def channel_list(self, exclude_archived: bool, exclude_members: bool,
                     limit: int) -> dict:
        """Lists all channels in a Slack team.

        This method returns a list of all channels in the team.
        This includes channels the caller is in, channels they are not currently in,
        and archived channels but does not include private channels.
        The number of (non-deactivated) members in each channel is also returned.

        """

        # prepare requests channels list to Slack API
        host = "{}/channels.list".format(conf.SLACK_HOST)
        params = {
            "token": conf.SLACK_LEGACY_TOKEN,
            "exclude_archived": exclude_archived,
            "exclude_members": exclude_members,
            "limit": limit
        }
        req = requests.get(host, params=params)

        # parsing response to json
        res_json = req.json()

        # handle requests
        response = {}
        if "error" in res_json:
            response["data"] = res_json["error"]
            response["error"] = True

        else:
            response["data"] = res_json["channels"]
            response["error"] = False

        # convert response to object
        response = json.loads(json.dumps(response))

        return response

    def channel_info(self, channel: str, include_locale: bool) -> dict:

        # prepare requests channels info to Slack API
        host = "{}/channels.info".format(conf.SLACK_HOST)
        params = {
            "token": conf.SLACK_LEGACY_TOKEN,
            "channel": channel,
            "include_locale": include_locale
        }
        req = requests.get(host, params=params)

        # parsing response to json
        res_json = req.json()

        # handle requests
        response = {}
        if "error" in res_json:
            response["data"] = res_json["error"]
            response["error"] = True

        else:
            response["data"] = res_json["channel"]
            response["error"] = False

        # convert response to object
        response = json.loads(json.dumps(response))

        return response

    def channel_invite(self, json_data: dict) -> dict:

        # validation json data
        try:
            channel_id = json_data["channel_id"]
        except:
            raise BadRequest("channel_id is missing", 200, 1)

        try:
            user_id = json_data["user_id"]
        except:
            raise BadRequest("user_id is missing", 200, 1)

        # prepare requests invite user to channel
        host = "{}/channels.invite".format(conf.SLACK_HOST)
        data = {
            "token": conf.SLACK_LEGACY_TOKEN,
            "channel": channel_id,
            "user": user_id
        }
        req = requests.post(host, data)

        # parsing response to json
        res_json = req.json()

        # handle requests
        response = {}
        if "error" in res_json:
            response["data"] = res_json["error"]
            response["error"] = True

        else:
            response["data"] = res_json["channel"]
            response["error"] = False

        # convert response to object
        response = json.loads(json.dumps(response))

        return response

    def channel_rename(self, json_data: dict) -> dict:

        # validation json data and set default params
        try:
            channel_id = json_data["channel_id"]
        except:
            raise BadRequest("channel_id is missing", 200, 1)

        try:
            name = json_data["name"]
        except:
            raise BadRequest("name is missing", 200, 1)

        try:
            validate = json_data["validate"]
        except:
            validate = False

        # prepare requests rename of channel
        host = "{}/channels.rename".format(conf.SLACK_HOST)
        data = {
            "token": conf.SLACK_LEGACY_TOKEN,
            "channel": channel_id,
            "name": name,
            "validate": validate
        }
        req = requests.post(host, data)

        # parsing response to json
        res_json = req.json()

        # handle requests
        response = {}
        if "error" in res_json:
            response["data"] = res_json["error"]
            response["error"] = True

        else:
            response["data"] = res_json["channel"]
            response["error"] = False

        # convert response to object
        response = json.loads(json.dumps(response))

        return response

    def channel_archive(self, json_data: dict) -> dict:
        # validation json data
        try:
            channel_id = json_data["channel_id"]
        except:
            raise BadRequest("channel_id is missing", 200, 1)

        # prepare requests archived channel
        host = "{}/channels.archive".format(conf.SLACK_HOST)
        data = {"token": conf.SLACK_LEGACY_TOKEN, "channel": channel_id}
        req = requests.post(host, data)

        # parsing response to json
        res_json = req.json()

        # handle requests
        response = {}
        if "error" in res_json:
            response["data"] = res_json["error"]
            response["error"] = True
        else:
            response["error"] = False

        # convert response to object
        response = json.loads(json.dumps(response))

        return response

    def channel_unarchive(self, json_data: dict) -> dict:
        # validation json data
        try:
            channel_id = json_data["channel_id"]
        except:
            raise BadRequest("channel_id is missing", 200, 1)

        # prepare requests unarchive channel
        host = "{}/channels.unarchive".format(conf.SLACK_HOST)
        data = {"token": conf.SLACK_LEGACY_TOKEN, "channel": channel_id}
        req = requests.post(host, data)

        # parsing response to json
        res_json = req.json()

        # handle requests
        response = {}
        if "error" in res_json:
            response["data"] = res_json["error"]
            response["error"] = True
        else:
            response["error"] = False

        # convert response to object
        response = json.loads(json.dumps(response))

        return response
