from flask import Blueprint, jsonify, request

from controllers import ChannelController
from helpers import Helper
from helpers.decorator import secret_key_required

ch_ctrlr = ChannelController()
helper = Helper()

bp = Blueprint(__name__, 'channels')


@bp.route("/channels/create", methods=["POST"])
@secret_key_required()
def channels_create():
    """Create a channel

    example ::
        >> curl -i -X GET
            -H "Content-Type: application/json"
            -H "x-api-key: <app secret key>
            -d '{"channel_name": "channel_001"}'
            http://localhost:5000/channels/create

    endpoint ::
        GET /channels/create

    **success response**

    .. sourcecode:: http
        HTTP/1.1 200 OK
        Content-Type: text/javascript

        {
            "error": 0,
            "data": [],
            "message": "Success"
        }

    """
    json_request = request.get_json(silent=True)

    channels_create, err = ch_ctrlr.channel_create(json_request)

    # default response format
    response = {"error": 0, "message": "Success", "data": []}

    if channels_create is False:
        response["error"] = 1
        response["message"] = err

    return jsonify(response)


@bp.route("/channels/list", methods=["get"])
@secret_key_required()
def channels_list():
    """Get list of channels

    example ::
        >> curl -i -X GET
            -H "Content-Type: application/json"
            -H "x-api-key: <app secret key>"
            http://localhost:5000/channels/list

    endpoint ::
        GET /channels/list

    **success response**

    .. sourcecode:: http
        HTTP/1.1 200 OK
        Content-Type: text/javascript

        {
            "data": [
                {
                    "created": 1527476070,
                    "creator": "XXXXXXXX",
                    "id": "CAX3BN612",
                    "is_archived": false,
                    "is_channel": true,
                    "is_general": false,
                    "is_member": true,
                    "is_mpim": false,
                    "is_org_shared": false,
                    "is_private": false,
                    "is_shared": false,
                    "members": [
                        "XXXXXXXX",
                        "XXXXXXXX"
                    ],
                    "name": "random",
                    "name_normalized": "random",
                    "num_members": 2,
                    "previous_names": [],
                    "purpose": {
                        "creator": "XXXXXXXX",
                        "last_set": 1527476070,
                        "value": "A place for non-work-related flimflam, faffing, hodge-podge or jibber-jabber you'd prefer to keep out of more focused work-related channels."
                    },
                    "topic": {
                        "creator": "UAX0DND99",
                        "last_set": 1527476070,
                        "value": "Non-work banter and water cooler conversation"
                    },
                    "unlinked": 0
                },
                {
                    "created": 1527476070,
                    "creator": "XXXXXXXX",
                    "id": "XXXXXXXX",
                    "is_archived": false,
                    "is_channel": true,
                    "is_general": true,
                    "is_member": true,
                    "is_mpim": false,
                    "is_org_shared": false,
                    "is_private": false,
                    "is_shared": false,
                    "members": [
                        "XXXXXXXX",
                        "XXXXXXXX"
                    ],
                    "name": "general",
                    "name_normalized": "general",
                    "num_members": 2,
                    "previous_names": [],
                    "purpose": {
                        "creator": "XXXXXXXX",
                        "last_set": 1527476070,
                        "value": "This channel is for workspace-wide communication and announcements. All members are in this channel."
                    },
                    "topic": {
                        "creator": "XXXXXXXX",
                        "last_set": 1527476070,
                        "value": "Company-wide announcements and work-based matters"
                    },
                    "unlinked": 0
                }
            ],
            "error": 0,
            "message": "Success requests list channels"
        }


    """

    # validation args and set defaults params
    try:
        exclude_archived = request.args.get("exclude_archived")
    except:
        exclude_archived = False

    try:
        exclude_members = request.args.get("exclude_members")
    except:
        exclude_members = False

    try:
        limit = request.args.get("limit")
    except:
        limit = 0

    # get channels list
    channels_list = ch_ctrlr.channel_list(exclude_archived, exclude_members,
                                          limit)

    # default response format
    response = {"error": 0, "data": [], "message": "Success"}

    if channels_list["error"] is True:
        response["error"] = 1
        response["message"] = channels_list["data"]
    else:
        response["data"] = channels_list["data"]

    return jsonify(response)


@bp.route("/channels/info/<channel_id>", methods=["get"])
@secret_key_required()
def channels_info(channel_id):
    """Get info channels by ID

    example ::
        >> curl -i -X GET
            -H "Content-Type: application/json"
            -H "x-api-key: <app secret key>"
            http://localhost:5000/channels/info/<channels_id>?include_locale=True

    endpoint ::
        GET /channels/info

    **success response**

    .. sourcecode:: http
        HTTP/1.1 200 OK
        Content-Type: text/javascript

        {
            "data": {
                "created": 1529567589,
                "creator": "XXXXXXXXX",
                "id": "XXXXXXXXX",
                "is_archived": true,
                "is_channel": true,
                "is_general": false,
                "is_member": false,
                "is_mpim": false,
                "is_org_shared": false,
                "is_private": false,
                "is_shared": false,
                "locale": "en-US",
                "members": [],
                "name": "channel_001",
                "name_normalized": "channel_001",
                "previous_names": [],
                "purpose": {
                    "creator": "",
                    "last_set": 0,
                    "value": ""
                },
                "topic": {
                    "creator": "",
                    "last_set": 0,
                    "value": ""
                },
                "unlinked": 0
            },
            "error": 0,
            "message": "Success"
        }

    """

    # validation args and set defaults params
    try:
        include_locale = request.args.get("include_locale")
    except:
        include_locale = False

    # get channel info by id channel
    channels_info = ch_ctrlr.channel_info(channel_id, include_locale)

    # default response format
    response = {"error": 0, "data": [], "message": "Success"}

    if channels_info["error"] is True:
        response["error"] = 1
        response["message"] = channels_info["data"]
    else:
        response["data"] = channels_info["data"]

    return jsonify(response)


@bp.route("/channels/invite", methods=["POST"])
@secret_key_required()
def channels_invite():
    """Invite user to channel

    example ::
        >> curl -i -X POST
            -H "Content-Type: application/json"
            -H "x-api-key: <app secret key>"
            -d '{
                "channel_id": "XXXXXXXXX",
                "user_id": "XXXXXXXXX"
            }'
        http://localhost:5000/channels/invite

    endpoint ::
        POST /channels/invite

    **success response**

    .. sourcecode:: http
        HTTP/1.1 200 OK
        Content-Type: text/javascript

        {
            "data": {
                "created": 1528353961,
                "creator": "XXXXXXXXX",
                "id": "XXXXXXXXX",
                "is_archived": false,
                "is_channel": true,
                "is_general": false,
                "is_member": true,
                "is_mpim": false,
                "is_org_shared": false,
                "is_private": false,
                "is_shared": false,
                "last_read": "1528354547.000263",
                "latest": {
                    "subtype": "channel_topic",
                    "text": "<@XXXXXXXXX> set the channel topic: for sharing task",
                    "topic": "for sharing task",
                    "ts": "1528354547.000263",
                    "type": "message",
                    "user": "XXXXXXXXX"
                },
                "members": [
                    "XXXXXXXXX"
                ],
                "name": "test",
                "name_normalized": "test",
                "previous_names": [],
                "priority": 0,
                "purpose": {
                    "creator": "",
                    "last_set": 0,
                    "value": ""
                },
                "topic": {
                    "creator": "XXXXXXXXX",
                    "last_set": 1528354547,
                    "value": "for sharing task"
                },
                "unlinked": 0,
                "unread_count": 0,
                "unread_count_display": 0

            },
            "error": 0,
            "message": "Success"
        }

    """

    json_request = request.get_json(silent=True)

    # invite user to channel
    channels_invite = ch_ctrlr.channel_invite(json_request)

    # default response format
    response = {"error": 0, "data": [], "message": "Success"}

    if channels_invite["error"] is True:
        response["error"] = 1
        response["message"] = channels_invite["data"]
    else:
        response["data"] = channels_invite["data"]

    return jsonify(response)


@bp.route("/channels/rename", methods=["POST"])
@secret_key_required()
def channels_rename():
    """Rename name of channel

    example ::
        >> curl -i -X POST
            -H "Content-Type: application/json"
            -H "x-api-key: <app secret key>"
            -d '{
                "channel_id": "XXXXXXXXX",
                "name": "XXXXXXXXX",
                "validate": false
            }'
            http://localhost:5000/channels/rename

    endpoint ::
        POST /channels/rename

    **success response**

    .. sourcecode:: http
        HTTP/1.1 200 OK
        Content-Type: text/javascript

        {
            "data": {
                "created": 1528353961,
                "creator": "XXXXXXXXX",
                "id": "XXXXXXXXX",
                "is_archived": false,
                "is_channel": true,
                "is_general": false,
                "is_member": false,
                "is_mpim": false,
                "is_org_shared": false,
                "is_private": false,
                "is_shared": false,
                "members": [],
                "name": "test_rename",
                "name_normalized": "test_rename",
                "previous_names": [
                    "test"
                ],
                "purpose": {
                    "creator": "",
                    "last_set": 0,
                    "value": ""
                },
                "topic": {
                    "creator": "XXXXXXXXX",
                    "last_set": 1528354547,
                    "value": "for sharing task"
                },
                "unlinked": 0
            },
            "error": 0,
            "message": "Success"
        }

    """

    json_request = request.get_json(silent=True)

    # rename name of channel
    channels_rename = ch_ctrlr.channel_rename(json_request)

    # default response format
    response = {"error": 0, "data": [], "message": "Success"}

    if channels_rename["error"] is True:
        response["error"] = 1
        response["message"] = channels_rename["data"]
    else:
        response["data"] = channels_rename["data"]

    return jsonify(response)


@bp.route("/channels/archive", methods=["POST"])
@secret_key_required()
def channels_archive():
    """Archive channel

    example ::
        >> curl -i -X POST
            -H "Content-Type: application/json"
            -H "x-api-key: <app secret key>"
            -d '{"channel_id": "XXXXXXXXX"}'
            http://localhost:5000/channels/archive

    endpoint ::
        POST /channels/archive

    **success response**

    .. sourcecode:: http
        HTTP/1.1 200 OK
        Content-Type: text/javascript

        {
            "data": [],
            "error": 0,
            "message": "Success"
        }

    """

    json_request = request.get_json(silent=True)

    # archive channel
    channels_archive = ch_ctrlr.channel_archive(json_request)

    # default response format
    response = {"error": 0, "data": [], "message": "Success"}

    if channels_archive["error"] is True:
        response["error"] = 1
        response["message"] = channels_archive["data"]

    return jsonify(response)


@bp.route("/channels/unarchive", methods=["POST"])
@secret_key_required()
def channels_unarchive():
    """Unarchive channel

    example ::
        >> curl -i -X POST
            -H "Content-Type: application/json"
            -H "x-api-key: <app secret key>"
            -d '{"channel_id": "XXXXXXXXX"}'
            http://localhost:5000/channels/unarchive

    endpoint ::
        POST /channels/unarchive

    **success response**

    .. sourcecode:: http
        HTTP/1.1 200 OK
        Content-Type: text/javascript

        {
            "data": [],
            "error": 0,
            "message": "Success"
        }

    """

    json_request = request.get_json(silent=True)

    # archive channel
    channels_archive = ch_ctrlr.channel_unarchive(json_request)

    # default response format
    response = {"error": 0, "data": [], "message": "Success"}

    if channels_archive["error"] is True:
        response["error"] = 1
        response["message"] = channels_archive["data"]

    return jsonify(response)
